selenium==3.14.0
termcolor==1.1.0
terminaltables==3.1.0
xmlrunner==1.7.7
locust==0.0
testinfra==3.0.5
ansible==2.8.1
paramiko
