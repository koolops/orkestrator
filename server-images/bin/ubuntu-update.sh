#!/bin/bash

#
# script: ubuntu-update.sh
# author: jorge.medina@kronops.com.mx
# desc: Update system packeges

# Enable debug mode and log to file
export DEBUG=1
LOG=/var/log/ubuntu-update.log
[ -n "$DEBUG" ] && exec < /dev/stdin > $LOG 2>&1

# Bash debug mode
[ -n "$DEBUG" ] && set -x

# Stop on errors
#set -e

echo "==> Disabling the release upgrader"
sed -i.bak 's/^Prompt=.*$/Prompt=never/' /etc/update-manager/release-upgrades

echo "==> Disable systemd apt timers and services"
systemctl stop apt-daily.timer;
systemctl stop apt-daily-upgrade.timer;
systemctl disable apt-daily.timer;
systemctl disable apt-daily-upgrade.timer;
systemctl mask apt-daily.service;
systemctl mask apt-daily-upgrade.service;
systemctl daemon-reload;

echo "==> Disable apt periodic routines"
cat <<EOF >/etc/apt/apt.conf.d/10periodic;
APT::Periodic::Enable "0";
APT::Periodic::Update-Package-Lists "0";
APT::Periodic::Download-Upgradeable-Packages "0";
APT::Periodic::AutocleanInterval "0";
APT::Periodic::Unattended-Upgrade "0";
EOF

echo "==> Remove unattended-upgrades"
rm -rf /var/log/unattended-upgrades;
apt-get -y purge unattended-upgrades;

echo "==> Updating list of repositories"
apt-get -y update

echo "==> Upgrade all packages and kernel"
apt-get -y dist-upgrade -o Dpkg::Options::="--force-confnew";

echo "Rebooting the system..."
reboot
