#!/bin/bash

#
# script: rhel-update.sh
# author: jorge.medina@kronops.com.mx
# desc: Update system packeges

# Enable debug mode and log to file
export DEBUG=1
LOG=/var/log/rhel-update.log
[ -n "$DEBUG" ] && exec < /dev/stdin > $LOG 2>&1

# Bash debug mode
[ -n "$DEBUG" ] && set -x

# Stop on errors
#set -e

echo "==> Updating list of repositories"
yum check-update

echo "==> Applying updates"
yum -y update

echo "Rebooting the system..."
reboot
