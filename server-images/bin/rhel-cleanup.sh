#!/bin/bash

#
# script: rhel-cleanup.sh
# author: jorge.medina@kronops.com.mx
# desc: Clean temporary packages, settings and files

# Enable debug mode and log to file
export DEBUG=1
LOG=/var/log/rhel-cleanup.log
[ -n "$DEBUG" ] && exec < /dev/stdin > $LOG 2>&1

# Bash debug mode
[ -n "$DEBUG" ] && set -x

# Stop on errors
#set -e

# main

echo '==> Cleanup udev network interfaces persistance'
rm -f /etc/udev/rules.d/70-persistent-net.rules;
mkdir -p /etc/udev/rules.d/70-persistent-net.rules;
rm -f /lib/udev/rules.d/75-persistent-net-generator.rules;
rm -rf /dev/.udev/;

echo '==> Removing HWADDR and UUID parameters for network interfaces'
if [ -f /etc/sysconfig/network-scripts/ifcfg-eth0 ] ; then
    sed -i "/^HWADDR/d" /etc/sysconfig/network-scripts/ifcfg-eth0
    sed -i "/^UUID/d" /etc/sysconfig/network-scripts/ifcfg-eth0
fi

echo "==> Rebuild & clean RPM database"
rpmdb --rebuilddb
rm -f /var/lib/rpm/__db*

echo "==> Remove Bash history"
unset HISTFILE
rm -f /root/.bash_history
rm -f /home/sysadmin/.bash_history

echo "==> Clear core files"
rm -f /core*

echo "==> Removing /tmp/* files"
rm -rf /tmp/*

echo "==> Removing /var/log/*.log files"
find /var/log/ -name *.log -exec rm -f {} \;

echo "==> Clean last login events"
>/var/log/lastlog
>/var/log/wtmp
>/var/log/btmp
