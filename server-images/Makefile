#
# Makefile for Server Image Builder
#
# author: jorge.medina@kronops.com.mx
# desc: Script to build the server images using packer for server environments.

IMAGE_NAME_UBUNTU1804 = template-ubuntu1804-x64-server.qcow2
IMAGE_NAME_UBUNTU1804_QCOW = template-ubuntu1804-x64-server.qcow
IMAGE_NAME_UBUNTU2004 = template-ubuntu2004-x64-server.qcow2
IMAGE_NAME_UBUNTU2004_QCOW = template-ubuntu2004-x64-server.qcow
IMAGE_NAME_CENTOS7 = template-centos7-x64-server.qcow2
IMAGE_NAME_CENTOS7_QCOW = template-centos7-x64-server.qcow
IMAGE_NAME_RHEL7 = template-rhel7-x64-server.qcow2
IMAGE_NAME_RHEL7_QCOW = template-rhel7-x64-server.qcow
IMAGE_NAME_RHEL8 = template-rhel8-x64-server.qcow2
IMAGE_NAME_RHEL8_QCOW = template-rhel8-x64-server.qcow
IMAGE_NAME_ORACLELINUX7 = template-oraclelinux7-x64-server.qcow2
IMAGE_NAME_ORACLELINUX7_QCOW = template-oraclelinux7-x64-server.qcow
IMAGES_REPOSITORY=/kvm/staging/server-images

.PHONY: all validate build-ubuntu1804 build-ubuntu2004 build-centos7 build-rhel7 build-rhel8 build-oraclelinux7 publish-ubuntu1804 publish-ubuntu2004 publish-centos7 publish-rhel7 publish-rhel8 publish-oraclelinux7 clean help

all: help

validate:
	packer validate ubuntu1804.json
	packer validate ubuntu2004.json
	packer validate centos7.json
	packer validate rhel7.json
	packer validate rhel8.json
	packer validate oraclelinux7.json

build-ubuntu1804:
	packer build ubuntu1804.json

build-ubuntu2004:
	packer build ubuntu2004.json

build-centos7:
	packer build centos7.json

build-rhel7:
	packer build rhel7.json

build-rhel8:
	packer build rhel8.json

build-oraclelinux7:
	packer build oraclelinux7.json

publish-ubuntu1804:
	cp builds/libvirt/${IMAGE_NAME_UBUNTU1804}/${IMAGE_NAME_UBUNTU1804} ${IMAGES_REPOSITORY}

publish-ubuntu2004:
	cp builds/libvirt/${IMAGE_NAME_UBUNTU2004}/${IMAGE_NAME_UBUNTU2004} ${IMAGES_REPOSITORY}

publish-centos7:
	cp builds/libvirt/${IMAGE_NAME_CENTOS7}/${IMAGE_NAME_CENTOS7} ${IMAGES_REPOSITORY}

publish-rhel7:
	cp builds/libvirt/${IMAGE_NAME_RHEL7}/${IMAGE_NAME_RHEL7} ${IMAGES_REPOSITORY}

publish-rhel8:
	cp builds/libvirt/${IMAGE_NAME_RHEL8}/${IMAGE_NAME_RHEL8} ${IMAGES_REPOSITORY}

publish-oraclelinux7:
	cp builds/libvirt/${IMAGE_NAME_ORACLELINUX7}/${IMAGE_NAME_ORACLELINUX7} ${IMAGES_REPOSITORY}

clean:
	@echo ""
	@echo "Cleaning local development environment."
	@echo ""
	rm -rf builds/*/*.box
	rm -rf builds/libvirt/*
	rm -rf packer_cache

help:
	@echo ""
	@echo "Please use \`make <target>' where <target> is one of"
	@echo ""
	@echo "  validate								Validates the config file."
	@echo "  build-ubuntu1804				Builds the ubuntu1804 server image."
	@echo "  build-ubuntu2004				Builds the ubuntu2004 server image."
	@echo "  build-centos7					Builds the centos7 server image."
	@echo "  build-rhel7						Builds the rhel7 server image."
	@echo "  build-rhel8						Builds the rhel8 server image."
	@echo "  build-oraclelinux7			Builds the oraclelinux7 server image."
	@echo "  publish-ubuntu1804			Publishes ubuntu1804 server image to staging repository."
	@echo "  publish-ubuntu2004			Publishes ubuntu2004 server image to staging repository."
	@echo "  publish-centos7				Publishes centos7 server image to staging repository."
	@echo "  publish-rhel7					Publishes rhel7 server image to staging repository."
	@echo "  publish-rhel8					Publishes rhel8 server image to staging repository."
	@echo "  publish-oraclelinux7		Publishes oraclelinux7 server image to staging repository."
	@echo "  clean									Cleans local changes and temporary filess."
	@echo ""
	@echo ""
