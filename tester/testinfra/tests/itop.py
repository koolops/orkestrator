#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from unittest import TestCase
from defs import functions as f

class Itop(TestCase): 
    def test_services(s):
        services = ['apache2','mysql']
        f.test_service_running(s.host, services, s.output)
        f.test_service_enabled(s.host, services, s.output)

    def test_pkgs(s):
        packages = [
         'apache2','apache2-bin','libapache2-mod-php7.0','libapache2-mod-php7.0',
         'php7.0-mysql',
         'mysql-server-5.7','mysql-client-5.7', 'mysql-server-core-5.7']
        f.test_pkgs(s.host, packages, s.output)

    def test_ports(s):
        ports = ['127.0.0.1:3306/tcp',
                 ':::80/tcp']
        f.test_ports(s.host, ports, s.output)
