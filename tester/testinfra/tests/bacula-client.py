#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from unittest import TestCase
from defs import functions as f

class Bacula-Client(TestCase):
    def test_pkgs(s):
        packages = ['bacula-client','mysql-client']
        f.test_pkgs(s.host, packages, s.output)

    def test_file_exist(s):
        files = [
            '/etc/bacula/bacula-fd.conf',
            '/root/bin',
            '/root/bin/dpkg-selections.sh',
            '/root/bin/mysql-simplebackup',
            '/var/local/mysqlbkdir',
            '/root/.my.cnf'
        ]
        f.test_file_exist(s.host, files, s.output)

    def test_services(s):
        services = ['bacula-fd']
        f.test_service_running(s.host, services, s.output)
        f.test_service_enabled(s.host, services, s.output)
