# encoding: utf-8

ip = command('hostname')
control ip.stdout  do
  title 'Check for services'
  describe service('cron') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
  describe service('sshd') do
    puts ip.stdout
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
  describe service('acpid') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end
