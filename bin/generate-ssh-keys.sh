#!/bin/bash

#
# script: generate-ssh-keys.sh
# author: Jorge Armando Medina
# desc: Generate SSH RSA 4096 keys for ansible and jenkins.

# Enable debug mode and log to file
export DEBUG=1
LOG=/var/log/generate-ssh-keys.log
[ -n "$DEBUG" ] && exec < /dev/stdin > $LOG 2>&1

# Bash debug mode
[ -n "$DEBUG" ] && set -x

# Stop on errors
#set -e

# vars
source .env

# main

# Create ssh directory
pwd
mkdir ansible/inventory/${PROJECT_NAME}/.ssh
chmod 700 ansible/inventory/${PROJECT_NAME}/.ssh
cd ansible/inventory/${PROJECT_NAME}/.ssh

echo "Generating RSA/4096 SSH keys for ansible user."
ssh-keygen -t rsa -b 4096 -C "ansible@${PROJECT_DOMAIN}" -f id-${PROJECT_NAME}-ansible.rsa -q -N ""

echo "Generating RSA/4096 SSH keys for jenkins user."
ssh-keygen -t rsa -b 4096 -C "jenkins@${PROJECT_DOMAIN}" -f id-${PROJECT_NAME}-jenkins.rsa -q -N ""

echo "Changing key permissions to 600."
chmod 600 *
