#!/bin/bash

#
# script: bootstrap.sh
# author: jorge.medina@kronops.com.mx
# desc: Installs ansible on ubuntu from ppa & prepares the environment for infrastructure, application and database deployments.

# Enable debug mode and log to file
export DEBUG=1
LOG=/var/log/bootstrap.log
[ -n "$DEBUG" ] && exec < /dev/stdin > $LOG 2>&1

# Bash debug mode
[ -n "$DEBUG" ] && set -x

# Stop on errors
set -e

# vars
source .env
# Disable errors from dpkg-reconfigure
export DEBIAN_FRONTEND=noninteractive

# main

echo "Updating apt lists."
sudo apt-get -qq update

echo "Installing basic requirements."
sudo apt-get -qq install vim curl git ssh aptitude update-notifier-common expect

echo "Installing software-properties-common."
sudo apt-get -qq install software-properties-common

echo "Installing Ansible dependencies."
sudo apt-get install -qq -y python3-dev python3-yaml python3-jinja2 python3-crypto python3-simplejson python3-setuptools sshpass

echo "Installing ansible from ansible ppa repository."
sudo apt install -qq -y python3-pip
sudo pip3 install ansible

echo "Creating and changing ownership and permissions for /var/lib/ansible."
mkdir -p /var/lib/ansible/retries
chown -R root:adm /var/lib/ansible
chmod 770 /var/lib/ansible/retries

echo "Changing ansible ownership and permissions for /var/log/ansible"
mkdir -p /var/log/ansible
chown root:adm /var/log/ansible
chmod 770 /var/log/ansible
touch /var/log/ansible/ansible.log
chown root:adm /var/log/ansible/ansible.log
chmod 660 /var/log/ansible/ansible.log

echo "Deploying Ansible infrastructure code."
rm -rf /etc/ansible
ln -s $PROJECT_ROOT/ansible /etc

echo "Cleaning previous Jenkins home installation."
[[ -d "/var/lib/jenkins"  ]] && cp -a /var/lib/jenkins /var/lib/jenkins-$(date +%Y_%m_%d_%S)
rm -rf /var/lib/jenkins

echo "Create docker auth folder"
mkdir $PROJECT_ROOT/.docker
echo "$DOCKER_PWD" > $PROJECT_ROOT/.docker/auth
