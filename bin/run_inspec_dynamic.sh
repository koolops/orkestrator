#!/bin/bash

source /var/lib/jenkins/.env

PROFILE="$1"
GROUP="$2"
KEY=/etc/ansible/inventory/$PROJECT_NAME/.ssh/id-${PROJECT_NAME}-ansible.rsa
USER=root
GET_DYNAMIC_INVENTORY=/opt/$PROJECT_ROOT/bin/get_dynamic_inventory.sh
INSPEC_OUT=$PROJECT_ROOT/tester/inspec/xmlresult
[[ -d "$WORKSPACE" ]] && OUT_PATH=$WORKSPACE || OUT_PATH=$INSPEC_OUT

if [[ ! -f "$KEY" ]]; then
    echo "\nNo SSH Key found\n"
    exit 1
fi

bash $GET_DYNAMIC_INVENTORY $GROUP > ips
cat ips | grep \|\ | cut -f1 -d "|" > ips_tmp
sed 's/ //g' -i ips_tmp
sed 's/"//g' -i ips_tmp
sed 's/\x1b\[[0-9;]*m//g' -i ips_tmp
while read -r line || [[ -n "$line" ]]; do
  echo ''
  inspec exec $PROFILE -t ssh://$USER@$line -i $KEY --reporter cli junit:$OUT_PATH/${line}-stdout.xml
done < ips_tmp
rm ips*

exit 0
